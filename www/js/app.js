// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova', 'ngStorage', 'xml'])

    .run(function ($ionicPlatform, $rootScope, $cordovaGeolocation, $http, $ionicPopup, $state, $localStorage) {

        $rootScope.lat = 0;
        $rootScope.lng = 0;
        $rootScope.address = "";
        $rootScope.uuid = "0";
        $rootScope.isGPSenabled = false;
        $rootScope.X = 0;
        $rootScope.currState = $state;
        $rootScope.State = '';
        $rootScope.absence = $localStorage.absence;

        $rootScope.$watch('currState.current.name', function (newValue, oldValue) {
            $rootScope.State = newValue;
        });

        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

        });

        // Get uuid

        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            $rootScope.uuid = device.uuid;
        }

    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'

            })

            .state('management', {
                url: '/management',
                templateUrl: 'templates/management.html',
                controller: 'ManagementCtrl'

            });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');
    });
