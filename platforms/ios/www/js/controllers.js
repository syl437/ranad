angular.module('starter.controllers', [])

    .controller('LoginCtrl', function (x2js, $scope, $ionicPopup, $rootScope, $http, $state, $localStorage, $ionicHistory, $httpParamSerializerJQLike,$timeout) {

            if (!$localStorage.phone || $localStorage.phone == ""){

                $scope.login = {

                    "phone" : ""

                };

                $scope.makeLogin = function () {

                    if ($scope.login.phone == "") {

                        $ionicPopup.alert({
                            title: "נא להזין כל השדות",
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    } else {

                        // $http.post("http://62.219.199.131/RanadMobileWS/Service1.asmx/IsUserAllowed", $httpParamSerializerJQLike({"UserId": $scope.login.phone}),
                        $http.post("http://62.219.199.131/RanadMobileWSTest/Service1.asmx/IsUserAllowed", $httpParamSerializerJQLike({"UserId": $scope.login.phone}),
                            {

                                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                paramSerializer: '$httpParamSerializer'

                            })

                            .then(function (data) {

                                var answer = x2js.xml_str2json(data.data);
                                answer = JSON.parse(answer.string.toString())[0];
                                console.log(answer);

                                $localStorage.response = answer.var.substr(answer.var.indexOf("var")+3, 1);
                                $localStorage.absence = answer.absence;
                                $rootScope.absence = $localStorage.absence;

                                alert($localStorage.response + " - " + $localStorage.absence);

                                switch ($localStorage.response) {

                                    case "0":     // false

                                        $ionicPopup.alert({
                                            title: "מצטערים, אתה לא יכול לדווח עם מספר הטלפון הזה",
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                        break;

                                    case "1":     // true without GPS

                                        $localStorage.phone = $scope.login.phone;

                                        $state.go("management");

                                        $scope.login = {

                                            "phone" : ""

                                        };

                                        break;

                                    case "2":     // Exception

                                        $ionicPopup.alert({
                                            title: "התקבלה שגיאה בבקשה נסה שנית",
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                        break;

                                    case "3":     // true with GPS

                                        $localStorage.phone = $scope.login.phone;

                                        $state.go("management");

                                        $scope.login = {

                                            "phone" : ""

                                        };

                                        break;

                                    default:

                                        $ionicPopup.alert({
                                            title: "התקבלה שגיאה בבקשה נסה שנית",
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                        break;
                                }

                            }, function (error) {

                                $ionicPopup.alert({
                                    title: "אין התחברות!",
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                });

                            });

                    }

                }

            } else {

                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });

                $state.go("management");

            }

    //    });

    })

    .controller('ManagementCtrl', function ($ionicLoading, x2js, $state, $scope, $rootScope, $ionicPopup, $http, $httpParamSerializerJQLike, $localStorage,$cordovaGeolocation,$timeout,$ionicPlatform) {

        $ionicPlatform.ready(function () {

            $timeout(function() {

                $scope.checkStatus();

            }, 100);

        });

        document.addEventListener("resume", onResumeStart, false);

        function onResumeStart() {

            if ($rootScope.State == "management") {

                $scope.checkStatus();

            }
        }

        $scope.numbers = {

            "project": "0",
            "task": "0"

        };

        $scope.sendReport = function (x) {

            if (($scope.numbers.project != "" && $scope.numbers.project != "0") && x == "32"){

                x = "41";

            }

            if (($scope.numbers.project != "" && $scope.numbers.project != "0") && x == "33"){

                x = "42";

            }

            if ($scope.numbers.project == "") {

                $scope.numbers.project = "0";

            } else if ($scope.numbers.task == "") {

                $scope.numbers.task = "0";

            }

            if ($localStorage.response === "3") {           // if the user can report only with GPS

                if (window.cordova) {

                    $ionicPlatform.ready(function () {

                        CheckGPS.check(function win() {

                                $ionicLoading.show({

                                    template: 'טוען...'

                                }).then(function () {

                                    var posOptions = {timeout: 5000, enableHighAccuracy: false};

                                    $cordovaGeolocation
                                        .getCurrentPosition(posOptions)
                                        .then(function (position) {

                                            $rootScope.isGPSenabled = true;
                                            $rootScope.lat = position.coords.latitude;
                                            $rootScope.lng = position.coords.longitude;
                                            $scope.getLocationName($rootScope.lat, $rootScope.lng);

                                            $scope.sendData(x);
                                            $ionicLoading.hide();

                                        }, function (err) {

                                            $rootScope.isGPSenabled = false;
                                            $rootScope.lat = 0;
                                            $rootScope.lng = 0;

                                            $ionicLoading.hide();

                                            $ionicPopup.alert({
                                                title: "עליך להדליק שירותי מיקום על מנת לשלוח דיווח",
                                                buttons: [{
                                                    text: 'שלח',
                                                    type: 'button-positive'
                                                }]
                                            });

                                        });

                                });

                            },

                            function fail() {       // if there is no GPS

                                var GPSalert = $ionicPopup.confirm({            // show alert
                                    title: 'שירותי מיקום מכובים',
                                    template: "עליך להדליק שירותי מיקום על מנת לשלוח דיווח"
                                });

                                GPSalert.then(function (res) {

                                    if (res) {

                                        if (ionic.Platform.isIOS()) {        // if the user chose to switch on GPS and it's IOS - get his deals

                                            cordova.plugins.diagnostic.switchToSettings(function () {}, function () {});

                                            document.addEventListener("resume", onResumeIOS, false);

                                            function onResumeIOS() {

                                                $ionicLoading.show({

                                                    template: 'טוען...'

                                                }).then(function () {

                                                    var posOptions = {timeout: 5000, enableHighAccuracy: false};

                                                    $cordovaGeolocation
                                                        .getCurrentPosition(posOptions)
                                                        .then(function (position) {

                                                            $rootScope.isGPSenabled = true;
                                                            $rootScope.lat = position.coords.latitude;
                                                            $rootScope.lng = position.coords.longitude;
                                                            $scope.getLocationName($rootScope.lat, $rootScope.lng);

                                                            $scope.sendData(x);
                                                            $ionicLoading.hide();
                                                            document.removeEventListener("resume", onResumeIOS);

                                                        }, function (err) {

                                                            $rootScope.isGPSenabled = false;
                                                            $rootScope.lat = 0;
                                                            $rootScope.lng = 0;

                                                            $ionicLoading.hide();
                                                            document.removeEventListener("resume", onResumeIOS);

                                                            $ionicPopup.alert({
                                                                title: "הקליטה חלשה מדי, אנא נסה שוב",
                                                                buttons: [{
                                                                    text: 'שלח',
                                                                    type: 'button-positive'
                                                                }]
                                                            });

                                                        });

                                                });
                                            }


                                        } else {        // if the user chose to switch on GPS and it's Android - get his deals

                                            cordova.plugins.diagnostic.switchToLocationSettings();

                                            document.addEventListener("resume", onResumeAndroid, false);

                                            function onResumeAndroid() {

                                                $ionicLoading.show({

                                                    template: 'טוען...'

                                                }).then(function () {

                                                    var posOptions = {timeout: 5000, enableHighAccuracy: false};

                                                    $cordovaGeolocation
                                                        .getCurrentPosition(posOptions)
                                                        .then(function (position) {

                                                            $rootScope.isGPSenabled = true;
                                                            $rootScope.lat = position.coords.latitude;
                                                            $rootScope.lng = position.coords.longitude;
                                                            $scope.getLocationName($rootScope.lat, $rootScope.lng);

                                                            $scope.sendData(x);
                                                            $ionicLoading.hide();
                                                            document.removeEventListener("resume", onResumeAndroid);

                                                        }, function (err) {

                                                            $rootScope.isGPSenabled = false;
                                                            $rootScope.lat = 0;
                                                            $rootScope.lng = 0;

                                                            $ionicLoading.hide();
                                                            document.removeEventListener("resume", onResumeAndroid);

                                                            $ionicPopup.alert({
                                                                title: "הקליטה חלשה מדי, אנא נסה שוב",
                                                                buttons: [{
                                                                    text: 'שלח',
                                                                    type: 'button-positive'
                                                                }]
                                                            });

                                                        });

                                                });

                                            }

                                        }

                                    } else {        // if the user doesn't want to switch on GPS

                                        $rootScope.isGPSenabled = false;

                                    }
                                });

                            });

                    });

                } else {

                    var posOptions = {timeout: 5000, enableHighAccuracy: false};

                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function (position) {

                            $rootScope.isGPSenabled = true;
                            $rootScope.lat = position.coords.latitude;
                            $rootScope.lng = position.coords.longitude;
                            $scope.getLocationName($rootScope.lat, $rootScope.lng);

                            $scope.sendData(x);
                            $ionicLoading.hide();

                        }, function (err) {

                            $rootScope.isGPSenabled = false;
                            $rootScope.lat = 0;
                            $rootScope.lng = 0;

                            $ionicLoading.hide();

                            $ionicPopup.alert({
                                title: "הקליטה חלשה מדי, אנא נסה שוב",
                                buttons: [{
                                    text: 'שלח',
                                    type: 'button-positive'
                                }]
                            });

                        });


                }

            } else if ($localStorage.response === "0"){         // if the user can't report at all

                $ionicPopup.alert({
                    title: "מצטערים, אתה לא יכול לדווח עם מספר הטלפון הזה",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($localStorage.response === "2") {        // if there is something wrong

                $ionicPopup.alert({
                    title: "התקבלה שגיאה בבקשה תיכנס שוב",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {            // if he can report without GPS

                $scope.sendData(x);

            }

		};

        $scope.getLocationName = function (lat, lng) {

            $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&sensor=true')

                .then(function (data) {

                    $rootScope.address = data.data.results.length > 0 ? data.data.results[0].formatted_address : 'גוגל לא מזהה כתובת נוכחית';

                }, function (err) {

                });
        };

        $scope.sendData = function(x) {

            $scope.fields =
                {
                    "UserId": $localStorage.phone,
                    "SugTnua": x,
                    "location": $rootScope.address,
                    "longitude": $rootScope.lng,
                    "latitude": $rootScope.lat,
                    "ProjectID": $scope.numbers.project,
                    "MesimaID": $scope.numbers.task,
                    "PeulaID" : "0",
                    "PhoneUniqueID": "d7ed911949ffe47a"
                    // "PhoneUniqueID": "d74b4290b010ca14"
                    // "PhoneUniqueID": "649d74b8fe654b1"
                    // "PhoneUniqueID": $rootScope.uuid
                };

            if($localStorage.response === "1") {
                $scope.fields.location = "";
                $scope.fields.longitude = 0;
                $scope.fields.latitude = 0;
            }

            alert(JSON.stringify($scope.fields));

            $http.post("http://62.219.199.131/RanadMobileWS/Service1.asmx/MikumMovesSave", $httpParamSerializerJQLike({

                UserId:$localStorage.phone,
                SugTnua:$scope.fields.SugTnua,
                location:$scope.fields.location,
                longitude:$scope.fields.longitude,
                latitude:$scope.fields.latitude,
                ProjectID:$scope.fields.ProjectID,
                MesimaID:$scope.fields.MesimaID,
                PeulaID:$scope.fields.PeulaID,
                PhoneUniqueID:$scope.fields.PhoneUniqueID

            }), {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                paramSerializer: '$httpParamSerializer'

            }).then(

                function (data) {

                    // alert(JSON.stringify(data.data));
                    $scope.answer = data.data.substr(data.data.indexOf("var") + 3, 1);
                    // alert($scope.answer);

                    switch ($scope.answer) {

                        case "0":     // The data is not saved at the database

                            $ionicPopup.alert({
                                title: "הנותנים לא נשמרו, אנא דווח ממכשירך המקורי",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;

                        case "1":     // true without GPS

                            $ionicPopup.alert({
                                title: "!הפעולה בוצעה בהצלחה",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            $scope.numbers = {

                                "project": "0",
                                "task": "0"

                            };

                            $scope.fields = {

                                "UserId": "0",
                                "SugTnua": "0",
                                "location": "",
                                "longitude": "0",
                                "latitude": "0",
                                "ProjectID": "0",
                                "MesimaID": "0",
                                "PhoneUniqueID": "0"

                            };

                            break;

                        case "2":     // Exception

                            $ionicPopup.alert({
                                title: "אירעה שגיאה ,אנא נסה שנית",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;

                        default:

                            $ionicPopup.alert({
                                title: "אירעה שגיאה ,אנא נסה שנית",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;
                    }

                }, function (error) {

                    $ionicPopup.alert({
                        title: "!אין התחברות",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                });

        };

        // Logout

        $scope.logout = function(){

            $localStorage.phone = "";
            $state.go('login');

        };

        $scope.checkStatus = function(){

            // $http.post("http://62.219.199.131/RanadMobileWS/Service1.asmx/IsUserAllowed", $httpParamSerializerJQLike({"UserId": $localStorage.phone}),
            $http.post("http://62.219.199.131/RanadMobileWSTest/Service1.asmx/IsUserAllowed", $httpParamSerializerJQLike({"UserId": $localStorage.phone}),
                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function (data) {

                    var answer = x2js.xml_str2json(data.data);
                    answer = JSON.parse(answer.string.toString())[0];
                    console.log(answer);

                    $localStorage.response = answer.var.substr(answer.var.indexOf("var")+3, 1);
                    $localStorage.absence = answer.absence;
                    $rootScope.absence = $localStorage.absence;

                    switch ($localStorage.response) {

                        case "0":     // false

                            $ionicPopup.alert({
                                title: "מצטערים, אתה לא יכול לדווח עם מספר הטלפון הזה",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;

                        case "1":     // true without GPS

                            break;

                        case "2":     // Exception

                            $ionicPopup.alert({
                                title: "התקבלה שגיאה בבקשה תיכנס שוב",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;

                        case "3":     // true with GPS

                            break;

                        default:

                            $ionicPopup.alert({
                                title: "התקבלה שגיאה בבקשה תיכנס שוב",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            break;
                    }

                }, function (error) {

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                });

        };

    });
